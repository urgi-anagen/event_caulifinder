#! /usr/bin/env python
# coding: utf-8

"""
Created on Thu Apr 25 15:13:52 2019
@author: S. Hadad
Created on Thu Jun 25 18:02:12 2020
@author: Houssem
Created on Tue Jun 16 19:27:41 2020
@author: Houssem

Modifications
@author: V. Jamilloux
@author: F. Maumus
"""

import os
import re
import shutil
import time
import subprocess
import multiprocessing
from argparse import ArgumentParser, RawTextHelpFormatter
import distutils.util

from commons.tools.ShortenConsensusHeader import ShortenConsensusHeader
from commons.core.utils.FileUtils import FileUtils
from commons.core.seq.FastaUtils import FastaUtils
from commons.launcher.LaunchBlastclust import LaunchBlastclust

def main():
    description = "Description:\n\n" \
                    "Caulifinder_branch_A.py aims at constructing a library of consensus sequences that are representative" \
                  " of repetitive elements with significant homology to Caulimoviridae found in a plant genome.\n" \
                  "This library typically contains complete Caulimoviridae genomes, almost complete genomes and genomic fragments.\n   \n\n"

    parser = ArgumentParser(description=description, formatter_class=RawTextHelpFormatter, add_help=True)

    mand = parser.add_argument_group("MANDATORY settings")

    mand.add_argument("-g", "--genome", help="Input genome file in fasta format", required = True)

    mand.add_argument("-l", "--library", help = "Reference Caulimoviridae genomes library file in fasta format\n", required = True)
                        
    parser.add_argument("-e", "--evalue", default = "1e-3", help="evalue for tBLASTx of reference Caulimoviridae genomes vs plant genome (default 1e-3)\n")


    mand.add_argument("-b", "--baits",
                        help = "Bank containing translated ORFs from Caulimoviridae genomes and LTR retrotransposons, e.g.: baits.fa\n", required = True)

    mand.add_argument("-c","--classiflib",
                        help = "Classified library of Caulimovirideae sequences in fasta format, e.g.: baits.fa\n", required = True )

    parser.add_argument("-r","--rps_domains", default = "cddid_all.tbl",
                        help = "Table with Id and names of the domains in CDD library used with rpsblast (default 'cddid_all.tbl')\n")

    mand.add_argument("-db","--database_rpsblast",
                        help= "database for rpsblast (absolute path and name), e.g.: Cdd_db/Cdd\n", required= True)

    parser.add_argument("-S", "--blastclust_identity", default = "90", help = "identity pct for blastclust (default 90)\n")

    parser.add_argument("-L", "--blastclust_coverage", default = "0.9", help = "Coverage rate for blastclust (default 0.9)\n")

    parser.add_argument("-X", "--extension", default = 2000,
                        help = "extension value, length of upstream and downstream merged hit extension, in bp (default 2000)\n")

    parser.add_argument("-ch", "--filter_chimeras",  type=distutils.util.strtobool, default = True,
                        help = "Filter consensus with potential TE domains (gag, integrase ...)\n\
These consensus sequences will be listed in FilteredChimeras.txt (default True)") 

    parser.add_argument("-hsp", "--hsp_number", type=distutils.util.strtobool, default = True,
                        help = "Minimum number of high-scoring pairs in a group to build a consensus, either 5 (default True) else 3 (False)\n") 

    parser.add_argument("-bl", "--blastclust_supplementation", type=distutils.util.strtobool, default = False,
                        help = "Add to final selection all the sequences from the blastclust clusters which contain at least one consensus classified as Caulimoviridae (default False)\n\
/!\\ Be careful, if True, this option may lead to the selection of false positives, for exploratory use only /!\\")



    args = parser.parse_args()
    lib = args.library
    genome = args.genome
    evalue =args.evalue
    baits=args.baits
    classiflib = args.classiflib
    rps_domains = args.rps_domains
    database_rpsblast = args.database_rpsblast
    blastclust_identity = args.blastclust_identity
    blastclust_coverage = args.blastclust_coverage
    extension = args.extension
    chimeras = args.filter_chimeras
    hsp_number = args.hsp_number
    output = "reflib_vs_genome" #tblastx output file
    blastclust_supplementation = args.blastclust_supplementation
    nbCpusUtils = multiprocessing.cpu_count() - 1
    print("Pipeline Caulifinder branch A starts {} on genome : {}. \n Parallelized treatments will be on {} cpus".format(time.strftime("%Y-%m-%d %H:%M:%S"), genome, nbCpusUtils))
    if hsp_number :
        cmd = "sed -i 's/minNbSeqPerGroup:.*/minNbSeqPerGroup: 5/g' TEdenovo.cfg ".format(genome)
        print("\tLaunch: {}\n".format(cmd))
        subprocess.call(cmd, shell=True)
        cmd = "sed -i 's/Grouper_include:.*/Grouper_include: 4/g' TEdenovo.cfg ".format(genome)
        print("\tLaunch: {}\n".format(cmd))
        subprocess.call(cmd, shell=True)
    else :
        cmd = "sed -i 's/minNbSeqPerGroup:.*/minNbSeqPerGroup: 3/g' TEdenovo.cfg ".format(genome)
        print("\tLaunch: {}\n".format(cmd))
        subprocess.call(cmd, shell=True)
        cmd = "sed -i 's/Grouper_include:.*/Grouper_include: 2/g' TEdenovo.cfg ".format(genome)
        print("\tLaunch: {}\n".format(cmd))
        subprocess.call(cmd, shell=True)

    ################################### Step 1: Build a subgenome with potential locis #################################
    print("step 1: Build a subgenome with potential locis - {}".format(time.strftime("%Y-%m-%d %H:%M:%S")))
    cmd = "formatdb -i {} -p F -o T".format(genome)
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)

    cmd = "blastall -p tblastx -i {} -d {} -o {} -S 1 -a {} -m 8 -e {}".format(lib, genome, output, nbCpusUtils, evalue)
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)

    cmd = "awk -v OFS='\\t' '($9<$10) {{strandPlus=\"+\"; name=$2\"_\"$1\"_\"$9\"_\"$10; print $2, $9, $10, name, $12, strandPlus}}" \
          " ($9>$10) {{strandMoins=\"-\"; name=$2\"_\"$1\"_\"$10\"_\"$9; print $2, $10, $9, name, $12, strandMoins}}' {}> {}.bed".format(
        output, output)
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)

    cmd = "sort -k1,1 -k2,2n {}.bed > {}.bed.sorted".format(output, output)
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)

    cmd = "bedtools merge -i {}.bed.sorted -s > clustc_{}.bed".format(output, output)
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)

    cmd = "awk -v OFS='\\t' '{{print $1, $2, $3, $1\"_\"$2\"_\"$3, \"1\", $4}}' clustc_{}.bed > clustc_{}.bed.format".format(
        output, output)
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)

    cmd = "bedtools getfasta -name -fo clustc_{}.fa -fi {} -bed clustc_{}.bed.format".format(output, genome, output)
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)

    cmd = "formatdb -i {} -p T -o T".format(baits)
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)
    
    if classiflib != baits :
        cmd = "formatdb -i {} -p T -o T".format(classiflib)
        print("\tLaunch: {}\n".format(cmd))
        subprocess.call(cmd, shell=True)

    cmd = "blastall -p blastx -i clustc_{}.fa -d {} -o clustcRefLibVsGenome_vs_baits -a 4 -m 8 -e 1e-5".format(output,
                                                                                                               baits)
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)

    cmd = "awk '!x[$1]++' clustcRefLibVsGenome_vs_baits | grep 'FALSE' | cut -f 1 | awk -v OFS='\\t' '{print $1, 'FALSE'}' | sed 's/:/_/g' | sed 's/-/_/g' | sort > Putative_FP.list"
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)

    cmd = "join -1 1 -a 2 Putative_FP.list clustc_{}.bed.format | grep -v 'FALSE' | sed 's/ /\t/g' > clustc_{}_filtered.bed".format(
        output, output)
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)

    cmd = "sort -k1,1 -k2,2n clustc_{}_filtered.bed > clustc_{}_filtered.bed.sorted".format(output, output)
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)

    cmd = "awk '{if (($3 - $2) >= 200) { print }}'  clustc_reflib_vs_genome_filtered.bed.sorted > clust5k_CMVhits_200seq.bed"
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)

    cmd = "bedtools merge -i clust5k_CMVhits_200seq.bed -d 5000 > clust5k_CMVhits.bed"
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)


    cmd = "bedtools slop  -i clust5k_CMVhits.bed -g {}.fai -b {} > clust5k_CMVhits_pm2k.bed".format(genome, extension)
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)

    cmd = "bedtools getfasta -fi {} -fo clust5k_CMVhits_pm2k.fa -bed clust5k_CMVhits_pm2k.bed".format(genome)
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)

    ################################### Step 1 : End ###################################################################
    print("step 1 end - {}".format(time.strftime("%Y-%m-%d %H:%M:%S")))

    ################################### Step 2 : Repet (until step 4) consensus build. #################################
    print("step 2: Repet (until step 4) consensus build - {}".format(time.strftime("%Y-%m-%d %H:%M:%S")))

    cmd = "fasta_formatter -i clust5k_CMVhits_pm2k.fa -w 60 -o subgenome.fa"
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)

    cmd = "launch_TEdenovo.py -P subgenome -C TEdenovo.cfg -S 1234 -v 3 > tedenovo.txt"
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)

    ################################### Step 2 : End ###################################################################
    print("step 2 end - {}".format(time.strftime("%Y-%m-%d %H:%M:%S")))

    ################################### Step 3 : Consensus clustering and caracterization and ##########################
    print("step 3: Consensus caracterization and clustering - {}".format(time.strftime("%Y-%m-%d %H:%M:%S")))

    ################# Format Repet output files adding clustering tool name headers ###############################################
    outFastaFileName = "PilerConsensus.fa"
    iSCH = ShortenConsensusHeader("subgenome_Blaster_Piler_Map/subgenome_Blaster_Piler_Map_consensus.fa", outFastaFileName, "subgenome", "blaster", "Piler", "Map")
    iSCH.run()
    
    outFastaFileName = "ReconConsensus.fa"
    iSCH = ShortenConsensusHeader("subgenome_Blaster_Recon_Map/subgenome_Blaster_Recon_Map_consensus.fa", outFastaFileName, "subgenome", "blaster", "Recon", "Map")
    iSCH.run()
    outFastaFileName = "GrouperConsensus.fa"
    iSCH = ShortenConsensusHeader("subgenome_Blaster_Grouper_Map/subgenome_Blaster_Grouper_Map_consensus.fa", outFastaFileName, "subgenome", "blaster", "Grouper", "Map")
    iSCH.run()
    
    ################################ Consensus_ReconGrouperPiler.fa creation ###########################################
    REPETlibrary = "Consensus_GrouperPilerRecon.fa"
    lFiles = ["GrouperConsensus.fa", "PilerConsensus.fa", "ReconConsensus.fa"]
    FileUtils.catFilesFromList(lFiles, REPETlibrary, sort=False, skipHeaders = False, separator = "")
    dConsToLength = FastaUtils.getLengthPerHeader(REPETlibrary)
    
  
    ################################ blastx Consensus_ReconGrouperPiler against classiflib (=baits.fa) #############

    RepetLibVsClassifLib = "Consensus_GrouperPilerRecon_vs_Classif"
    cmd = "blastall -p blastx -i {} -d {} -o {} -a 4 -m 8 -e 1e-20".format(REPETlibrary, classiflib, RepetLibVsClassifLib)
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)

    ############################### Get consensus besthits from blastx tool ####################################
    print("Get consensus besthits in 'consBestHitsHeaders.txt' and make corresponding fasta file 'Caulimoviridae_BestHits.fa'.")
    selectedSeqFromConsBestHits_fastaFile = "Caulimoviridae_BestHits.fa"
    dConsToBestHit = getBesthits(RepetLibVsClassifLib, "consBestHitsHeaders.txt")
    FastaUtils.dbExtractByFilePattern("consBestHitsHeaders.txt", REPETlibrary, selectedSeqFromConsBestHits_fastaFile)

    ################################ retrieve more sequences with blastclust group between besthits and non besthits ###
    if blastclust_supplementation :

        dConsToBlastClustGroupAndMember, dBlastClustGroupToConsList = blastclustDirectory(REPETlibrary,blastclust_identity,blastclust_coverage,dConsToLength)
        ###### Selection sequences list of blastclust clusters in which there are one or several consensus besthits ########
        selectedSeq_fastaFile = best_hitClust_selection(REPETlibrary,dBlastClustGroupToConsList,dConsToBestHit,dConsToBlastClustGroupAndMember,genome)
    ################################ or just get clusters and sorted  best hits sequences #############
    else :

        dConsToBlastClustGroupAndMember, dBlastClustGroupToConsList = blastclustDirectory(selectedSeqFromConsBestHits_fastaFile,blastclust_identity,blastclust_coverage,dConsToLength)
        selectedSeq_fastaFile = best_hitClust_selection(REPETlibrary,dBlastClustGroupToConsList,dConsToBestHit,dConsToBlastClustGroupAndMember,genome)


    ############ RPSblast using selected sequences from 'selectedSeqHeaders.txt' against database_rpsblast #############
    print("rpstblastn using selected sequences from 'selectedSeqHeaders.txt' against {}.".format(database_rpsblast))
    print("\tStart at: {}".format(time.strftime("%Y-%m-%d %H:%M:%S")))
    
    rpsBlastOutput = "Caulimoviridae.rps"
    cmd = "rpstblastn -query {} -db {} -evalue 1e-3 -seg 'yes' -outfmt 6 -out {} -num_threads {}".format(selectedSeq_fastaFile,
                                                                           database_rpsblast, rpsBlastOutput, nbCpusUtils)
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)
    print("\tFinish at: {}".format(time.strftime("%Y-%m-%d %H:%M:%S")))

    ############################ Get domains list for each selected consensus ##########################################
    dIdToName_fromCddTblFile = convertFileDomainsToDict(rps_domains)
    dConsToDomainsList = extractDomainsList(rpsBlastOutput, dIdToName_fromCddTblFile)

    ############################ Get results tab #######################################################################
    out = "summary_table.csv"
    print("Build the results tab '{}'.".format(out))
    seqWithoutChimeras="ChimerasFreeHeaders.txt"
    seqWithPotentialChimeras="FilteredChimeras.txt"
    getResultsTab(out, dConsToBlastClustGroupAndMember, dConsToLength, dConsToDomainsList, dConsToBestHit, seqWithoutChimeras,seqWithPotentialChimeras , chimeras)

    ############################ Get consensus without chimeras from results tab ########################################
    if chimeras :
        FastaUtils.dbExtractByFilePattern("ChimerasFreeHeaders.txt", REPETlibrary, selectedSeq_fastaFile)
   

    ################################### Step 3 : end ###################################################################
    print("step 3 end - {}".format(time.strftime("%Y-%m-%d %H:%M:%S")))
    ################################### Step 4: Genome annotation with RepeatMasker ####################################
    

    print("step 4: Genome annotation with RepeatMasker using {} - {}".format(selectedSeq_fastaFile, time.strftime("%Y-%m-%d %H:%M:%S")))

    cmd = "RepeatMasker -lib {} -dir RepeatMasker -nolow -no_is -pa {} {} -gff".format(selectedSeq_fastaFile, nbCpusUtils, genome)
    print("\tLaunch: {}\n".format(cmd))
    subprocess.call(cmd, shell=True)


    #### summarize main files in a summary directory #################

    main_files = ["RepeatMasker/"+genome+".out.gff","RepeatMasker/"+genome+".tbl","summary_table.csv","Caulimoviridae_all.fa","FilteredChimeras.txt","Caulimoviridae_BestHits.fa","Caulimoviridae.rps"]
    if os.path.exists("summary_branchA"):
        shutil.rmtree("summary_branchA")
    os.makedirs("summary_branchA")
    #os.chdir("summary_branchA")
    for file in main_files:
        if os.path.exists(file):
            shutil.copy(file,"summary_branchA")
   
    cmd = "tar zcvf summary_branchA.tar.gz summary_branchA/"
    subprocess.call(cmd, shell=True)

    ################################### Step 4 : end ###################################################################
    print("step 4 end - {}".format(time.strftime("%Y-%m-%d %H:%M:%S")))
    print("Caulifinder branch A end")
    return 0

################################### Objects ############################################################################

class Hit:
    """
    characteric of a hit to choose the best one (with the biggest score)
    -----------
    -subject sequence : str 'subgenome_...'
                        the query sequence
    -query sequence : str 'Florendo_...'
                        the subject sequence corresponding to the query one
    -identity : float 
                the identity in percentage                   

    -score : int 
                the score which belong to the hit

    """
    def __init__(self, query, subject, identity, score, gypsy= False):
        self.query = query
        self.subject = subject
        self.identity = identity 
        self.score = score
        self.gypsy = gypsy





################################### All intern methods #################################################################

def blastclustDirectory(selectedSeq,idBlast, covBlast,dConsToLength):
    blastclustFolder = "blastclustFolder"
    if os.path.exists(blastclustFolder):
        shutil.rmtree(blastclustFolder)
    os.makedirs(blastclustFolder)
    os.chdir(blastclustFolder)
    os.symlink("../{}".format(selectedSeq), selectedSeq)

    iLaunchBlastclust = LaunchBlastclust(selectedSeq)
    iLaunchBlastclust.setIdentityThreshold(idBlast)
    iLaunchBlastclust.setCoverageThreshold(covBlast)
    iLaunchBlastclust.setBothSequences("F")
    iLaunchBlastclust.run()
    lHeaders = FastaUtils.dbHeaders("{}_Blastclust.fa".format(selectedSeq ))

    dConsToBlastClustGroupAndMember, dBlastClustGroupToConsList = dictsFromBlastClust(lHeaders)
    print("There are {} blastclust gro../RepeatMasker/*.gff ups".format(len(dConsToLength),  len(dBlastClustGroupToConsList)))
    os.chdir("..")
    return dConsToBlastClustGroupAndMember, dBlastClustGroupToConsList

def best_hitClust_selection(library,dBlastClustGroupToConsList,dConsToBestHit,dConsToBlastClustGroupAndMember,genome):
    print("Selection sequences list of blastclust clusters in 'selectedSeqHeaders.txt' and make corresponding fasta file 'Caulimoviridae_all.fa'.")
    lSelectedSeq = seqSelectionFromBestHits(dBlastClustGroupToConsList, dConsToBestHit, dConsToBlastClustGroupAndMember, "selectedSeqHeaders.txt")
    selectedSeq_fastaFile = "Caulimoviridae_all.fa"
    FastaUtils.dbExtractByFilePattern("selectedSeqHeaders.txt", library, selectedSeq_fastaFile)
    nbSelectedSequences = len(lSelectedSeq)
    if nbSelectedSequences == FastaUtils.dbSize(selectedSeq_fastaFile):
        print("\t{} fasta file contains {} all caulimoviridae from {} genome".format(selectedSeq_fastaFile, nbSelectedSequences, genome))
    else:
        print("\tWarnning: {} fasta file empty".format(selectedSeq_fastaFile))
    return selectedSeq_fastaFile

def seqSelectionFromBestHits(dBlastClustGroupToConsList, dConsToBestHit, dConsToBlastClustGroupAndMember, headersFile):
    lSelectedSeq = []
    lSelectedGroup = []
    for cons in dConsToBestHit.keys():
        if cons in dConsToBlastClustGroupAndMember.keys():
            blastclustGroup, blastclustMember = dConsToBlastClustGroupAndMember[cons]
            if blastclustGroup not in lSelectedGroup:
                lSelectedGroup.append(blastclustGroup)
                lSelectedSeq.extend(dBlastClustGroupToConsList[str(blastclustGroup)])
    if lSelectedSeq:
        with open(headersFile, "w") as f:
            for iHeader in lSelectedSeq:
                f.write("{}\n".format(iHeader))
    else:
        print("No sequences are selected and the {} file will not be created".format(headersFile))

    return lSelectedSeq

def getBesthits(inputfile, headersFile):
    print("besthit consensus extract from output blast file {}".format(inputfile))
    dConsName2BestHits = {}
    dBestScore = {}
    if os.path.exists(headersFile):
        os.remove(headersFile)
    with open( headersFile, 'w') as h:
        with open( inputfile, 'r') as f:

            prevName = ""
            for line in f:
                res = re.search('(subgenome_.*)\t(.*)\t(.*)\t.*\t.*\t.*\t.*\t.*\t.*\t.*\t.*\t(.*)',line)
                if res:
                    currentName = res.group(1)
                    if currentName != prevName:
                        prevID = 0
                        prevScore =0
                        prevName = currentName
                    currentCaulimoType = res.group(2)
                    identity_1 =  float(res.group(3))
                    score_1 = float(res.group(4))
                    res_FALSE=re.search('FALSE.+',currentCaulimoType) 
                    if score_1 >= prevScore :
                        if score_1 > prevScore or identity_1 > prevID:
                            if not res_FALSE and currentCaulimoType != 'Copia' and currentCaulimoType != 'Gypsy1' and currentCaulimoType != 'Gypsy2': 
        
                                dBestScore[currentName] = Hit(currentName, currentCaulimoType, identity_1, score_1,False)
                            else : 
                                 dBestScore[currentName] = Hit(currentName, currentCaulimoType, identity_1, score_1, True)
                        prevScore=score_1
                        prevID=identity_1

                  
                  
        for cName in dBestScore :
            if dBestScore[cName].gypsy == False:
                dConsName2BestHits[dBestScore[cName].query] = [dBestScore[cName].subject, dBestScore[cName].identity, dBestScore[cName].score]
                h.write("{}\n".format(dBestScore[cName].query))
            
            

    return dConsName2BestHits



def dictsFromBlastClust(lHeaders):
    dConsToBlastClustGroupAndMember = {}
    dBlastClustGroupToConsList = {}
    for iHeader in lHeaders:
        resultat = re.search('BlastclustCluster(.*)Mb(.*)_subgenome(.*)', iHeader)
        if resultat:
            groupBlastClust = resultat.group(1)
            memberBlastClust = resultat.group(2)
            consensusName = "subgenome{}".format(resultat.group(3))
            dConsToBlastClustGroupAndMember[consensusName] = (int(groupBlastClust), int(memberBlastClust))
            if groupBlastClust in dBlastClustGroupToConsList.keys():
                dBlastClustGroupToConsList[groupBlastClust].append(consensusName)
            else:
                dBlastClustGroupToConsList[groupBlastClust] = [consensusName]

    return dConsToBlastClustGroupAndMember, dBlastClustGroupToConsList

def extractDomainsList(RPSFileName, domainsDict):

    dConsToDomainsList = {}
    with open(RPSFileName, 'r') as f: # inputRPSFileName must be in rpsblast output format
        for l in f:
            res = re.search('(.*)\t.*\|.*\|(.*)\t.*\t.*\t.*\t.*\t.*\t.*\t.*\t.*\t.*\t.*', l)
            if res:
                cons = res.group(1)
                domainName = domainsDict[res.group(2)]
                if cons not in dConsToDomainsList.keys():
                    dConsToDomainsList[cons] = []
                if domainName not in dConsToDomainsList[cons]:
                    dConsToDomainsList[cons].append(domainName)
            else:
                res = re.search('(.*)\tCDD:(.*)\t.*\t.*\t.*\t.*\t.*\t.*\t.*\t.*\t.*\t.*', l)
                if res:
                    cons = res.group(1)
                    domainName = domainsDict[res.group(2)]
                    if cons not in dConsToDomainsList.keys():
                        dConsToDomainsList[cons] = []
                    if domainName not in dConsToDomainsList[cons]:
                        dConsToDomainsList[cons].append(domainName)

    print("Nb items in dict dConsToDomainsList: {}".format(len(dConsToDomainsList)))
    return dConsToDomainsList

def convertFileDomainsToDict(fileDomains):
# Build dIdToDomain from domainsTab file
    lProteaseDomains = ['retropepsin_like', 'PR', 'peptidase', 'Asp_protease_2', 'Asp_protease', 'Peptidase_A3']
    lRTDomains = ['RT_like', 'RVT_1', 'rve', 'RT_Rtv', 'RT_LTR', 'RVT_3', 'RT_DIRS1', 'RT_ZFREV_like', 'RT_pepA17']
    lRNaseHDomains = ['RNase_HI_RT_DIRS1', 'RNase_HI_RT_Ty3', ]
    dIdToDomain = {}
    with open(fileDomains, 'r') as f:
        for l in f:
            res = re.search('(.*)\t.*\t(.*)\t.*\t.*', l)
            domainName = res.group(2)
            if domainName in lProteaseDomains:
                domainName = 'protease'
            elif domainName in lRTDomains:
                domainName = 'RT'
            elif domainName in lRNaseHDomains:
                domainName = 'RNaseH'
            dIdToDomain[res.group(1)] = domainName
    return dIdToDomain
    
def confusedSequences(consName,domains):
        match = False
        transp_domains = ["gag","Retrotrans","Integrase","Integrs","transpos","DUF4219"]
       
        for gag in transp_domains:
            if any(re.search(gag,dom,re.IGNORECASE) for dom in domains):
                match = True
            
        return match

def getResultsTab(summaryFile, dicForBlastclustInfos, dictForSize, dictForDomains, dictForBesthit, seqWithoutChimeras, seqWithPotentialChimeras, chimera):

    print("Get results tab {}".format(summaryFile))
    with open(summaryFile, "w") as f, open(seqWithoutChimeras,"w") as rep, open (seqWithPotentialChimeras, "w") as ch:
        f.write("Consensus_sequence\tgroup\tsize\tdomains\tbest_hit\tid\tscore\n")
        group_change = 0
        with open("selectedSeqHeaders.txt", "r") as fCons:
            for cons in fCons:
                consName = cons.rstrip("\n")
                group = dicForBlastclustInfos[consName][0] #the first item of tuple corresponds to the group
                size = dictForSize[consName]
                if consName in dictForDomains.keys():
                    domains = dictForDomains[consName]
                else:
                    domains = []
                best_hit = ""
                ident = 0.0
                score = 0.0
                if consName in dictForBesthit.keys():
                    best_hit = dictForBesthit[consName][0]
                    ident = float(dictForBesthit[consName][1])
                    score = float(dictForBesthit[consName][2])
                


                match = False
                match = confusedSequences(consName,domains)
                if match:
                    ch.write(consName+"\n")  

                if chimera:
                    if not match :

                        line = "{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(consName, group, size, domains, best_hit, ident,
                                                                 score)
                        rep.write(consName+"\n")
                    else :

                        match = False
                


                else:



 
                    line = "{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(consName, group, size, domains, best_hit, ident,
                                                                 score)
                if line != "" :
                    f.write(line)
    print("Tab {} is ready".format(summaryFile))

    
                

if __name__ == "__main__":
    main()

