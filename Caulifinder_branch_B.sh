#! /bin/bash
#make symbolic link to genome in working directory

filename=$1

echo "Caulifinder_barch_B on $filename, START $(date)"
echo '#############  RT scan #############'
formatdb -p F -o T -i $filename
blastall -p tblastn -d $filename -i Caulimo_RT_probes.fa -o RTprobes_tBn -a 4 -m 8 -e 1e-5
awk '{n=$9+120; $15=n} {p=$10-120; $16=p} {y=$9-120; $17=y} {z=$10+120; $18=z} ($9>$10&&$16>0) {print $2 "\ttBn\tRT\t" $16 "\t" $15 "\t.\t-\t.\tID="$2"_"$9"_"$10} ($10>$9&&$17>0) {print $2 "\ttBn\tRT\t" $17 "\t" $18 "\t.\t+\t.\tID="$2"_"$9"_"$10}' RTprobes_tBn > RTprobes_tBn.gff3
echo '#############  clustering overlaping RT hits #############'
clusterize.py -i RTprobes_tBn.gff3 -f gff3 -o clust_RTprobes_tBn.gff3
echo '#############  extracting fasta nt sequence #############'
coordinatesToSequence.py -i clust_RTprobes_tBn.gff3 -f gff3 -s $filename -o clust_RTprobes_tBn.fa
sed 's/--.*//g' clust_RTprobes_tBn.fa > clust_RTprobes_tBn.fa.format
echo '#############  translating ORFs #############'
translate -l 200 clust_RTprobes_tBn.fa.format -o clust_RTprobes_tBn_200aa.fa
sed 's/  .*//g' clust_RTprobes_tBn_200aa.fa | sed 's/ //g' | sed 's/--/_/g' | sed 's/\./_/g' | sed 's/__/_/g' > clust_RTprobes_tBn_200aa.fa.format
formatdb -p T -o T -i Tree_RT_set.fa
blastall -p blastp -i clust_RTprobes_tBn_200aa.fa.format -d Tree_RT_set.fa -o clust_RTprobes_tBn_200aa_vs_RTprobes -a 4 -m 8 -e 1e-5
echo '#############  selecting CMV best hits of sufficient length #############'
awk '!x[$1]++' clust_RTprobes_tBn_200aa_vs_RTprobes | grep -v 'OUTGP' | awk '($8-$7>170) {print $0}'| cut -f 1  | sort -u > blast200_RTCMV_list_nr
restrictSequenceList.py -i clust_RTprobes_tBn_200aa.fa.format -f fasta -n blast200_RTCMV_list_nr -o blast_RTCMV.fa
sed 's/X//g' blast_RTCMV.fa > blast_RTCMV.fa.format
uclust --sort blast_RTCMV.fa.format --output blast_RTCMV_usort
uclust --input blast_RTCMV_usort --uc results.uc --id 0.80
uclust --uc2clstr results.uc --output results.clstr
uclust --uc2fasta results.uc --input blast_RTCMV_usort --output resultsID80.fasta
grep '*' resultsID80.fasta | sed 's/>//' > resultsID80_repheaders
restrictSequenceList.py -i resultsID80.fasta -f fasta -n resultsID80_repheaders -o resultsID80_rep.fa

sed 's/|\*|/_/g' resultsID80_rep.fa | sed 's/ .*//g' > resultsID80_rep.fa.format

echo '#############  Preparation step for next phylogeny step #############'
cat resultsID80_rep.fa.format Caulimo_RT_probes.fa > RefAndcandidates.fa
muscle -in RefAndcandidates.fa -out RefAndcandidates.fa.aln
trimal -in RefAndcandidates.fa.aln -out RefAndcandidates.fa.aln.trim50 -htmlout output1.html -resoverlap 0.75 -seqoverlap 50
muscle -in RefAndcandidates.fa.aln.trim50 -out RefAndcandidates.fa.aln.trim50.aln
trimal -in RefAndcandidates.fa.aln.trim50.aln -out RefAndcandidates.fa.aln.trim50.aln.trim -htmlout output2.html -gt 0.5
muscle -in RefAndcandidates.fa.aln.trim50.aln.trim -out RefAndcandidates.fa.aln.trim50.aln.trim.aln
#not sure why this last step is useful
trimal -in RefAndcandidates.fa.aln.trim50.aln.trim.aln -out RefAndcandidates.fa.aln.trim50.aln.trim.aln.trim -htmlout output3.html -resoverlap 0.75 -seqoverlap 50
grep '>' RefAndcandidates.fa.aln.trim50.aln.trim.aln.trim | grep -v 'OUTGP' | sed 's/>//g' | sed 's/ .*//g' > candidates_header_list
restrictSequenceList.py -i resultsID80_rep.fa.format -f fasta -n candidates_header_list -o candidate_CMVRT_aa.fa

echo '#############  Phylogeny step #############'
##quick and dirty phylogenies  !!try to do less rounds if diff n vs n-1 is null
cat Tree_RT_set.fa candidate_CMVRT_aa.fa > candidate_and_probes.fa
muscle -in candidate_and_probes.fa -out candidate_and_probes.fa.aln
trimal -in candidate_and_probes.fa.aln -out candidate_and_probes.fa.aln.trim -htmlout output4.html -resoverlap 0.75 -seqoverlap 60
muscle -in candidate_and_probes.fa.aln.trim -out candidate_and_probes.fa.aln.trim.aln
trimal -in candidate_and_probes.fa.aln.trim.aln -out candidate_and_probes.fa.aln.trim.aln.trim -htmlout output5.html  -resoverlap 0.75 -seqoverlap 70
muscle -in candidate_and_probes.fa.aln.trim.aln.trim -out candidate_and_probes.fa.aln.trim.aln.trim.aln
trimal -in candidate_and_probes.fa.aln.trim.aln.trim.aln -out candidate_and_probes.fa.aln.trim.aln.trim.aln.trim -htmlout output6.html  -resoverlap 0.85 -seqoverlap 70
muscle -in candidate_and_probes.fa.aln.trim.aln.trim.aln.trim -out candidate_and_probes.fa.aln.trim.aln.trim.aln.trim.aln
trimal -in candidate_and_probes.fa.aln.trim.aln.trim.aln.trim.aln -out candidate_and_probes.fa.aln.trim.aln.trim.aln.trim.aln.trim -htmlout output7.html  -resoverlap 0.75 -seqoverlap 75
muscle -in candidate_and_probes.fa.aln.trim.aln.trim.aln.trim.aln.trim -out candidate_and_probes.fa.aln.trim.aln.trim.aln.trim.aln.trim.aln
ln -s candidate_and_probes.fa.aln.trim.aln.trim.aln.trim.aln.trim.aln final_candidate_and_probes.fa.aln
Gblocks final_candidate_and_probes.fa.aln -t=p -b4=4 -b5=h
sed 's/ //g' final_candidate_and_probes.fa.aln-gb > final_MSA.fa
fasta2relaxedPhylip.pl -f final_MSA.fa -o final_MSA.fa.ph
phyml -i final_MSA.fa.ph -d aa

##out dir need to complete
mkdir summary_branchB
cd summary_branchB
cp ../resultsID80.fasta RT_clusters_all.fa
cp ../resultsID80_rep.fa.format Representative_RT.fa
cp ../candidate_CMVRT_aa.fa candidate_Caulimoviridae_RT.fa
cp ../final_MSA.fa .
cp ../final_MSA.fa.ph_phyml_tree.txt final_tree.txt

cd ..
tar zcvf summary_branchB.tar.gz summary_branchB/
echo "Caulifinder_branch_B on $filename, END $(date)"
