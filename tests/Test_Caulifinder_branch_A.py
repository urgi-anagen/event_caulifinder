import io
import os
import unittest
import filecmp
import Caulifinder_branch_A

#Class to describe unitary tests of all Caulifinder_branch_A.py intern methods
class Test_Caulifinder_branch_A(unittest.TestCase):

    def test_getBesthits(self):
        self._writeFile("blastResultsFile.tsv")
        self._writeConsHeadersFile()
        dCaulimoObs = Caulifinder_branch_A.getBesthits("blastResultsFile.tsv", "consBestHeaders.txt")
        dCaulomoExp = {"subgenome_blaster_Grouper_1009_Map_4": ['Florendovirus', '65.67', '322'],
                       "subgenome_blaster_Grouper_1013_Map_16": ['Florendovirus', '65.32', '234'],
                       "subgenome_blaster_Grouper_1019_Map_3": ['Unclassified_Vvin', '88.89', '113'],
                       "subgenome_blaster_Grouper_1094_Map_20": ['Florendovirus', '60.64', '111']}
        self.assertDictEqual(dCaulomoExp, dCaulimoObs)
        self.assertListEqual(list(io.open("ConsHeadersFileExp.txt")), list(io.open("consBestHeaders.txt")))
        os.remove("blastResultsFile.tsv")
        os.remove("ConsHeadersFileExp.txt")
        os.remove("consBestHeaders.txt")

    def test_dictsFromBlastClust(self):
        lHeaders = ["BlastclustCluster1Mb4_subgenome_blaster_Grouper_380_Map_4", "BlastclustCluster1Mb5_subgenome_blaster_Grouper_48_Map_20",
                    "BlastclustCluster1Mb6_subgenome_blaster_Recon_35_Map_20", "BlastclustCluster1Mb7_subgenome_blaster_Piler_47.24_Map_10",
                    "BlastclustCluster73Mb1_subgenome_blaster_Recon_323_Map_3", "BlastclustCluster73Mb2_subgenome_blaster_Grouper_553_Map_3",
                    "BlastclustCluster263Mb1_subgenome_blaster_Recon_141_Map_3",
                    "BlastclustCluster264Mb1_subgenome_blaster_Grouper_461_Map_3",
                    "BlastclustCluster265Mb1_subgenome_blaster_Recon_688_Map_5"]
        dConsToBlastClustGroupAndMemberObs, dBlastClustGroupToConsListObs = Caulifinder_branch_A.dictsFromBlastClust(lHeaders)
        dConsToBlastClustGroupAndMemberExp = {"subgenome_blaster_Grouper_380_Map_4": (1,4), "subgenome_blaster_Grouper_48_Map_20": (1,5),
                                              "subgenome_blaster_Recon_35_Map_20": (1, 6), "subgenome_blaster_Piler_47.24_Map_10": (1,7),
                                              "subgenome_blaster_Recon_323_Map_3": (73, 1), "subgenome_blaster_Grouper_553_Map_3": (73,2),
                                              "subgenome_blaster_Recon_141_Map_3": (263,1),
                                              "subgenome_blaster_Grouper_461_Map_3": (264, 1),
                                              "subgenome_blaster_Recon_688_Map_5": (265, 1)}
        dBlastClustGroupToConsListOExp = {"1": ["subgenome_blaster_Grouper_380_Map_4", "subgenome_blaster_Grouper_48_Map_20",
                                                "subgenome_blaster_Recon_35_Map_20", "subgenome_blaster_Piler_47.24_Map_10"],
                                          "73": ["subgenome_blaster_Recon_323_Map_3", "subgenome_blaster_Grouper_553_Map_3"],
                                          "263": ["subgenome_blaster_Recon_141_Map_3"],
                                          "264": ["subgenome_blaster_Grouper_461_Map_3"],
                                          "265": ["subgenome_blaster_Recon_688_Map_5"]}

        self.assertDictEqual(dConsToBlastClustGroupAndMemberExp, dConsToBlastClustGroupAndMemberObs)
        self.assertDictEqual(dBlastClustGroupToConsListOExp, dBlastClustGroupToConsListObs)

    def test_seqSelectionFromBestHits(self):
        dConsToBestHit = {"seq1": ['Florendovirus', '65.67', '322'], "seq2": ['Unclassified_Vvin', '88.89', '113'], "seq100": ['Toto', '60.64', '111']}
        dBlastClustGroupToConsList = {"1": ["seq1", "seq11", "seq2", "seq3"], "2": ["seq10", "seq5", "seq20"], "3": ["seq100"], "4": ["seq200"]}
        dConsToBlastClustGroupAndMember = {"seq1": (1, 1), "seq2": (1, 3), "seq11": (1, 2), "seq3": (1, 4),
                                           "seq10": (2, 1), "seq5": (2, 2), "seq20": (2, 3), "seq100": (3, 1), "seq200": (4, 1)}
        lSelectedSeqExp = ["seq1", "seq11", "seq2", "seq3", "seq100"]
        lSelectedSeqObs = Caulifinder_branch_A.seqSelectionFromBestHits(dBlastClustGroupToConsList, dConsToBestHit, dConsToBlastClustGroupAndMember,
                                 "selectedSeqHeaders.txt")
        self.assertListEqual(lSelectedSeqExp, lSelectedSeqObs)
        os.remove("selectedSeqHeaders.txt")

    def test_seqSelectionFromBestHits_bestOnlyInOneCluster(self):
        dConsToBestHit = {"seq1": ['Florendovirus', '65.67', '322'], "seq2": ['Unclassified_Vvin', '88.89', '113']}
        dBlastClustGroupToConsList = {"1": ["seq1", "seq2"], "2": ["seq10", "seq5", "seq20"], "3": ["seq100"], "4": ["seq200"]}
        dConsToBlastClustGroupAndMember = {"seq1": (1, 1), "seq2": (1, 2), "seq10": (2, 1), "seq5": (2, 2), "seq20": (2, 3),
                                           "seq100": (3, 1), "seq200": (4, 1)}
        lSelectedSeqExp = ["seq1", "seq2"]
        lSelectedSeqObs = Caulifinder_branch_A.seqSelectionFromBestHits(dBlastClustGroupToConsList, dConsToBestHit,
                                                                        dConsToBlastClustGroupAndMember, "selectedSeqHeaders.txt")
        self.assertListEqual(lSelectedSeqExp, lSelectedSeqObs)
        os.remove("selectedSeqHeaders.txt")

    def test_seqSelectionFromBestHits_emptyList(self):
        dConsToBestHit = {"seq1": ['Florendovirus', '65.67', '322'], "seq2": ['Unclassified_Vvin', '88.89', '113']}
        dBlastClustGroupToConsList = {"1": ["seq11", "seq3"], "2": ["seq10", "seq5", "seq20"], "3": ["seq100"], "4": ["seq200"]}
        dConsToBlastClustGroupAndMember = {"seq11": (1, 1), "seq3": (1, 2),
                                           "seq10": (2, 1), "seq5": (2, 2), "seq20": (2, 3), "seq100": (3, 1), "seq200": (4, 1)}
        lSelectedSeqExp = []
        lSelectedSeqObs = Caulifinder_branch_A.seqSelectionFromBestHits(dBlastClustGroupToConsList, dConsToBestHit, dConsToBlastClustGroupAndMember,
                                 "selectedSeqHeaders.txt")
        self.assertListEqual(lSelectedSeqExp, lSelectedSeqObs)

    def test_getConsClusteringInformation(self):
        lHeaders = ["subgenome_blaster_Grouper_1_Map_20", "subgenome_blaster_Recon_1013_Map_16",
                    "subgenome_blaster_Piler_47.24_Map_10", "subgenome_blaster_Piler_1.3_Map_20"]
        dObs = Caulifinder_branch_A.getConsClusteringInformation(lHeaders, "subgenome", "blaster")
        dExp = {"subgenome_blaster_Grouper_1_Map_20": ("Grouper", "1", "Map", "20"),
                "subgenome_blaster_Recon_1013_Map_16": ("Recon", "1013", "Map", "16"),
                "subgenome_blaster_Piler_47.24_Map_10": ("Piler", "47.24", "Map", "10"),
                "subgenome_blaster_Piler_1.3_Map_20": ("Piler", "1.3", "Map", "20")}
        self.assertDictEqual(dExp, dObs)

    def test_getNbSeqFromCluster(self):
        os.mkdir("subgenome_Blaster_Grouper")
        os.chdir("subgenome_Blaster_Grouper")
        self._writeGrouperFile()
        os.chdir("..")
        os.mkdir("subgenome_Blaster_Recon")
        os.chdir("subgenome_Blaster_Recon")
        self._writeReconFile()
        os.chdir("..")
        os.mkdir("subgenome_Blaster_Piler")
        os.chdir("subgenome_Blaster_Piler")
        self._writePilerFile()
        os.chdir("..")

        dCons = {"subgenome_blaster_Grouper_1_Map_20": ("Grouper", "1", "Map", "20"),
                 "subgenome_blaster_Grouper_2_Map_5": ("Grouper", "2", "Map", "5"),
                 "subgenome_blaster_Grouper_3_Map_20": ("Grouper", "3", "Map", "20"),
                 "subgenome_blaster_Recon_1_Map_3": ("Recon", "1", "Map", "3"),
                 "subgenome_blaster_Recon_2_Map_20": ("Recon", "2", "Map", "20"),
                 "subgenome_blaster_Recon_3_Map_20": ("Recon", "3", "Map", "20"),
                 "subgenome_blaster_Piler_0.41_Map_3": ("Piler", "0.41", "Map", "3"),
                 "subgenome_blaster_Piler_1.40_Map_20": ("Piler", "1.40", "Map", "20")}
        dObs = Caulifinder_branch_A.getNbSeqFromCluster(dCons)
        dExp = {"subgenome_blaster_Grouper_1_Map_20": 85,
                 "subgenome_blaster_Grouper_2_Map_5": 5,
                 "subgenome_blaster_Grouper_3_Map_20": 20,
                 "subgenome_blaster_Recon_1_Map_3": 3,
                 "subgenome_blaster_Recon_2_Map_20": 43,
                 "subgenome_blaster_Recon_3_Map_20": 406,
                 "subgenome_blaster_Piler_0.41_Map_3": 3,
                 "subgenome_blaster_Piler_1.40_Map_20": 47}

        os.chdir("subgenome_Blaster_Grouper")
        os.remove("subgenome.align.not_over.filtered.match.path.group.c0.95.txt")
        os.chdir("../subgenome_Blaster_Recon")
        os.remove("subgenome_Blaster_Recon.map.distribC")
        os.chdir("../subgenome_Blaster_Piler")
        os.remove("subgenome_Blaster_Piler.map.distribC")
        os.chdir("../")
        os.removedirs("subgenome_Blaster_Grouper")
        os.removedirs("subgenome_Blaster_Recon")
        os.removedirs("subgenome_Blaster_Piler")

        self.assertDictEqual(dExp, dObs)

    def test_convertFileDomainsToDict(self):
        self._writeDomainsFile("toto.tbl")
        expDict = {"238825": "RT", "333820": "RT", "133136": "protease", "275316": "Mplasa_alph_rch", "131117": "RNaseH"}
        obsDict = Caulifinder_branch_A.convertFileDomainsToDict("toto.tbl")
        os.remove("toto.tbl")

        self.assertDictEqual(expDict, obsDict)

    def test_extractDomainsList(self):
        rpsFileName = "matches.rps"
        self._writeRPSFile(rpsFileName)
        dIdToNameDomain = {"238825": "RT", "333820": "RT", "133136": "protease", "275316": "Mplasa_alph_rch", "131117": "RNaseH",
                           "279452": "MP", "376463": "Topoisom_bac"}
        dExp = {"toto": ["RT", "protease"], "titi": ["Mplasa_alph_rch", "RNaseH"], "Tata": ["MP", "RT"], "tutu": ["MP"], "toutou": ["Topoisom_bac"]}
        dObs = Caulifinder_branch_A.extractDomainsList(rpsFileName, dIdToNameDomain)
        os.remove(rpsFileName)

        self.assertDictEqual(dExp, dObs)

    def test_getResultsTab(self):
        resultsFileName = "resultsFile.csv"
        with open("selectedSeqHeaders.txt", "w") as f:
            f.write("subgenome_blaster_Recon_333_Map_5\n")
            f.write("subgenome_blaster_Grouper_1009_Map_4\n")
            f.write("subgenome_blaster_Piler_0.41_Map_3\n")
            f.write("subgenome_blaster_Grouper_213_Map_9\n")
            f.write("subgenome_blaster_Grouper_945_Map_20\n")
            f.write("subgenome_blaster_Grouper_134_Map_9\n")
            f.write("subgenome_blaster_Grouper_64_Map_20\n")
            f.write("subgenome_blaster_Recon_47_Map_20\n")

        dForBlastclust = {"subgenome_blaster_Recon_333_Map_5": (2, 45), "subgenome_blaster_Grouper_1009_Map_4": (2, 50),
                 "subgenome_blaster_Grouper_64_Map_20": (18, 5), "subgenome_blaster_Piler_0.41_Map_3": (4, 45),
                 "subgenome_blaster_Grouper_213_Map_9": (4, 23), "subgenome_blaster_Grouper_945_Map_20": (190, 1),
                 "subgenome_blaster_Grouper_134_Map_9": (18, 8), "subgenome_blaster_Recon_47_Map_20": (18, 11)}
        dForNbSeq = {"subgenome_blaster_Recon_333_Map_5": 5, "subgenome_blaster_Grouper_1009_Map_4": 4,
                 "subgenome_blaster_Grouper_64_Map_20": 26, "subgenome_blaster_Piler_0.41_Map_3": 3,
                 "subgenome_blaster_Grouper_213_Map_9": 9, "subgenome_blaster_Grouper_945_Map_20": 23,
                 "subgenome_blaster_Grouper_134_Map_9": 9, "subgenome_blaster_Recon_47_Map_20": 20}
        dForSize = {"subgenome_blaster_Recon_333_Map_5": 5326, "subgenome_blaster_Grouper_1009_Map_4": 2529,
                 "subgenome_blaster_Grouper_64_Map_20": 7184, "subgenome_blaster_Piler_0.41_Map_3": 5334,
                 "subgenome_blaster_Grouper_213_Map_9": 4002, "subgenome_blaster_Grouper_945_Map_20": 1854,
                 "subgenome_blaster_Grouper_134_Map_9": 2310, "subgenome_blaster_Recon_47_Map_20": 1954}
        dForDomains = {"subgenome_blaster_Recon_333_Map_5": ['RT', 'RNaseH', 'MP'], "subgenome_blaster_Grouper_1009_Map_4": ['RT', 'Mplasa_alph_rch'],
                       "subgenome_blaster_Grouper_64_Map_20": [], "subgenome_blaster_Piler_0.41_Map_3": ['RT', 'ND5', 'protease'],
                       "subgenome_blaster_Grouper_213_Map_9": [], "subgenome_blaster_Grouper_945_Map_20": ['RT', 'RNaseH'],
                        "subgenome_blaster_Grouper_134_Map_9": [], "subgenome_blaster_Recon_47_Map_20": ['RT', 'RT_Bac_retron_I', 'RNaseH']}
        dForBesthits = {"subgenome_blaster_Recon_333_Map_5": ['Florendovirus', '57.66', '274'], "subgenome_blaster_Grouper_1009_Map_4": ['Florendovirus', '65.67', '322'],
                        "subgenome_blaster_Piler_0.41_Map_3": ['Unclassified_Vvin', '94.92', '235'],
                        "subgenome_blaster_Grouper_213_Map_9": ['Fernendo1', '28.74', '160'], "subgenome_blaster_Grouper_945_Map_20": ['Florendovirus', '74.58', '194'],
                        "subgenome_blaster_Grouper_134_Map_9": ['Caulimovirus', '36.46', '70.9']}
        self._writeResultsFile("exp_{}".format(resultsFileName))

        Caulifinder_branch_A.getResultsTab(resultsFileName, dForBlastclust, dForNbSeq, dForSize, dForDomains, dForBesthits)
        self.assertTrue(filecmp.cmp("exp_{}".format(resultsFileName), resultsFileName))
        os.remove("exp_{}".format(resultsFileName))
        os.remove(resultsFileName)
        os.remove("selectedSeqHeaders.txt")

    def test_getResultsTab_bestOnlyInOneCluster(self):
        resultsFileName = "resultsFile.csv"
        with open("selectedSeqHeaders.txt", "w") as f:
            f.write("subgenome_blaster_Recon_333_Map_5\n")
            f.write("subgenome_blaster_Grouper_1009_Map_4\n")

        dForBlastclust = {"subgenome_blaster_Recon_333_Map_5": (2, 45), "subgenome_blaster_Grouper_1009_Map_4": (2, 50),
                 "subgenome_blaster_Grouper_64_Map_20": (18, 5), "subgenome_blaster_Piler_0.41_Map_3": (4, 45),
                 "subgenome_blaster_Grouper_213_Map_9": (4, 23), "subgenome_blaster_Grouper_945_Map_20": (190, 1),
                 "subgenome_blaster_Grouper_134_Map_9": (18, 8), "subgenome_blaster_Recon_47_Map_20": (18, 11)}
        dForNbSeq = {"subgenome_blaster_Recon_333_Map_5": 5, "subgenome_blaster_Grouper_1009_Map_4": 4,
                 "subgenome_blaster_Grouper_64_Map_20": 26, "subgenome_blaster_Piler_0.41_Map_3": 3,
                 "subgenome_blaster_Grouper_213_Map_9": 9, "subgenome_blaster_Grouper_945_Map_20": 23,
                 "subgenome_blaster_Grouper_134_Map_9": 9, "subgenome_blaster_Recon_47_Map_20": 20}
        dForSize = {"subgenome_blaster_Recon_333_Map_5": 5326, "subgenome_blaster_Grouper_1009_Map_4": 2529,
                 "subgenome_blaster_Grouper_64_Map_20": 7184, "subgenome_blaster_Piler_0.41_Map_3": 5334,
                 "subgenome_blaster_Grouper_213_Map_9": 4002, "subgenome_blaster_Grouper_945_Map_20": 1854,
                 "subgenome_blaster_Grouper_134_Map_9": 2310, "subgenome_blaster_Recon_47_Map_20": 1954}
        dForDomains = {"subgenome_blaster_Recon_333_Map_5": ['RT', 'RNaseH', 'MP'], "subgenome_blaster_Grouper_1009_Map_4": ['RT', 'Mplasa_alph_rch'],
                       "subgenome_blaster_Grouper_64_Map_20": [], "subgenome_blaster_Piler_0.41_Map_3": ['RT', 'ND5', 'protease'],
                       "subgenome_blaster_Grouper_213_Map_9": [], "subgenome_blaster_Grouper_945_Map_20": ['RT', 'RNaseH'],
                       "subgenome_blaster_Grouper_134_Map_9": [], "subgenome_blaster_Recon_47_Map_20": ['RT', 'RT_Bac_retron_I', 'RNaseH']}
        dForBesthits = {"subgenome_blaster_Recon_333_Map_5": ['Florendovirus', '57.66', '274'], "subgenome_blaster_Grouper_1009_Map_4": ['Florendovirus', '65.67', '322']}
        self._writeResultsFile_bestOnlyInOneCluster("exp_{}".format(resultsFileName))

        Caulifinder_branch_A.getResultsTab(resultsFileName, dForBlastclust, dForNbSeq, dForSize, dForDomains, dForBesthits)
        self.assertTrue(filecmp.cmp("exp_{}".format(resultsFileName), resultsFileName))
        os.remove("exp_{}".format(resultsFileName))
        os.remove(resultsFileName)
        os.remove("selectedSeqHeaders.txt")

    def _writeGrouperFile(self):
        with open("subgenome.align.not_over.filtered.match.path.group.c0.95.txt", "w") as f:
            f.write("GROUP NUMBER 1 CLUSTER 0 contains 85 members:\n")
            f.write("\tmember \tseq_num\tstart \tend  \tfrag \tlength \tinclude\ttotal \talign_list\n")
            f.write("1\t1\tchunk2807\t1053\t3532\t2\t2480\tfalse\t2\t44938 230346\n")
            f.write("1\t2\tchunk0539\t3696\t616\t2\t2407\tfalse\t1\t-230346\n")
            f.write("1\t3\tchunk0523\t2529\t4196\t1\t1668\tfalse\t4\t-20789 -29645 -43010 -229590\n")
            f.write("1\t4\tchunk0525\t273\t1928\t1\t1656\tfalse\t4\t20789 39208 -49656 -229591\n")
            f.write("GROUP NUMBER 2 CLUSTER 0 contains 5 members:\n")
            f.write("\tmember \tseq_num\tstart \tend  \tfrag \tlength \tinclude\ttotal \talign_list\n")
            f.write("2\t73186\tchunk0579\t1910\t3680\t1\t1771\ttrue\t1\t-132869\n")
            f.write("2\t73187\tchunk2013\t2427\t659\t1\t1769\ttrue\t6\t132869 139407 -124303 -132843 139407 -140320\n")
            f.write("GROUP NUMBER 3 CLUSTER 0 contains 20 members:\n")
            f.write("\tmember \tseq_num\tstart \tend  \tfrag \tlength \tinclude\ttotal \talign_list\n")
            f.write("3\t74341\tchunk0787\t5054\t5827\t1\t774\tfalse\t7\t-172064 -178800 191143 -197217 -207077 -214349 -219217\n")
            f.write("3\t74342\tchunk2094\t4444\t5227\t1\t784\tfalse\t1\t172064\n")
            f.write("3\t74343\tchunk2085\t3458\t4242\t1\t785\tfalse\t1\t178800\n")

    def _writeReconFile(self):
        with open("subgenome_Blaster_Recon.map.distribC", "w") as f:
            f.write("cluster\tsize\tmeanMbLength\tvarMbLength\tsdMbLength\tcvMbLength\n")
            f.write("1\t3\t1151.000000\t0.000000\t0.000000\t0.000000\n")
            f.write("2\t43\t3324.998197\t2288037.279780\t1512.625955\t0.454925\n")
            f.write("3\t406\t3744.674877\t2037295.642182\t1427.338657\t0.381165\n")

    def _writePilerFile(self):
        with open("subgenome_Blaster_Piler.map.distribC", "w") as f:
            f.write("cluster\tsize\tmeanMbLength\tvarMbLength\tsdMbLength\tcvMbLength\n")
            f.write("0.41\t3\t5385.666667\t12884.333333\t113.509177\t0.021076\n")
            f.write("1.40\t47\t4940.255319\t67489.281221\t259.786992\t0.052586\n")

    def _writeFile(self, inputFile):
        with open(inputFile, "w") as f:
            f.write("subgenome_blaster_Grouper_1_Map_20\tGypsy1\t63.11\t225\t83\t0\t1907\t2581\t1\t225\t8e-97\t303\n")
            f.write("subgenome_blaster_Grouper_1_Map_20\tGypsy2\t34.22\t225\t148\t2\t1907\t2581\t1\t206\t2e-36\t129\n")
            f.write("subgenome_blaster_Grouper_1_Map_20\tCaulimovirus\t29.23\t260\t183\t5\t1814\t2590\t230\t470\t1e-26\t106\n")
            f.write("subgenome_blaster_Grouper_1009_Map_4\tFlorendovirus\t65.67\t233\t80\t0\t700\t2\t2\t234\t1e-107\t322\n")
            f.write("subgenome_blaster_Grouper_1009_Map_4\tGymnendo4\t56.03\t232\t102\t1\t697\t2\t3\t233\t2e-85\t263\n")
            f.write("subgenome_blaster_Grouper_1009_Map_4\tGymnendo3\t54.55\t231\t105\t0\t694\t2\t4\t234\t1e-84\t261\n")
            f.write("subgenome_blaster_Grouper_1143_Map_20\tCopia\t45.08\t122\t66\t2\t364\t2\t11\t130\t5e-3\t112\n")
            f.write("subgenome_blaster_Grouper_1013_Map_16\tFlorendovirus\t65.32\t173\t60\t0\t2302\t1784\t77\t249\t5e-75\t234\n")
            f.write("subgenome_blaster_Grouper_1013_Map_16\tGymnendo4\t56.65\t173\t75\t1\t2302\t1784\t77\t248\t2e-61\t197\n")
            f.write("subgenome_blaster_Grouper_1019_Map_3\tUnclassified_Vvin\t88.89\t63\t7\t0\t2\t190\t165\t227\t4e-31\t113\n")
            f.write("subgenome_blaster_Grouper_10_Map_20\tGypsy2\t47.15\t123\t65\t1\t2082\t2450\t1\t122\t4e-40\t108\n")
            f.write("subgenome_blaster_Grouper_10_Map_20\tGypsy2\t36.84\t76\t48\t0\t2461\t2688\t126\t201\t4e-40\t66.2\n")
            f.write("subgenome_blaster_Grouper_10_Map_20\tCaulimovirus\t39.23\t130\t78\t1\t2058\t2444\t252\t381\t4e-36\t95.1\n")
            f.write("subgenome_blaster_Grouper_1094_Map_20\tFlorendovirus\t60.64\t94\t37\t0\t2102\t1821\t156\t249\t1e-30\t111\n")
            f.write("subgenome_blaster_Grouper_1094_Map_20\tGymnendo4\t48.45\t97\t50\t0\t2111\t1821\t152\t248\te-23\t90.5\n")
            f.write("subgenome_blaster_Grouper_1094_Map_20\tGymnendo3\t51.16\t86\t42\t0\t2114\t1857\t152\t237\t2e-23\t89.7\n")

    def _writeConsHeadersFile(self):
        with open("ConsHeadersFileExp.txt", "w") as f:
            f.write("subgenome_blaster_Grouper_1009_Map_4\n")
            f.write("subgenome_blaster_Grouper_1013_Map_16\n")
            f.write("subgenome_blaster_Grouper_1019_Map_3\n")
            f.write("subgenome_blaster_Grouper_1094_Map_20\n")

    def _writeDomainsFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("238825\tcd01647\tRT_LTR\tRT_LTR: Reverse transcriptases (RTs) from retrotransposons and retroviruses which have long terminal repeats (LTRs) in their DNA copies but not in their RNA template. RT catalyzes DNA replication from an RNA template.\t177\n")
            f.write("333820\tpfam00078\tRVT_1\tReverse transcriptase (RNA-dependent DNA polymerase). A reverse transcriptase gene is usually indicative of a mobile element such as a retrotransposon or retrovirus. Reverse transcriptases occur in a variety.\t185\n")
            f.write("275316\tTIGR04523\tMplasa_alph_rch\thelix-rich Mycoplasma protein. Members of this family occur strictly within a subset of Mycoplasma species.\t745\n")
            f.write("133136\tcd00303\tretropepsin_like\tRetropepsins; pepsin-like aspartate proteases.\t92\n")
            f.write("131117\tTIGR02062\tRNase_HI_RT_Ty3\texoribonuclease II. [Transcription, Degradation of RNA]\t639\n")

    def _writeRPSFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("toto\tgnl|CDD|238825\t36.52\t115\t68\t3\t1820\t1491\t3\t104\t5e-22\t 103\n")
            f.write("toto\tgnl|CDD|133136\t22.09\t86\t62\t2\t2509\t2267\t7\t91\t6e-06\t50.8\n")
            f.write("toto\tgnl|CDD|333820\t40.00\t50\t22\t1\t1613\t1488\t59\t108\t5e-09\t60.8\n")
            f.write("titi\tgnl|CDD|275316\t21.37\t117\t77\t4\t2945\t2640\t194\t307\t6e-04\t44.3\n")
            f.write("titi\tgnl|CDD|131117\t24.03\t129\t94\t5\t1671\t1297\t1\t120\t2e-08\t57.8\n")
            f.write("Tata\tCDD:279452\t22.840\t162\t115\t3\t3476\t3021\t16\t177\t1.40e-11\t64.7\n")
            f.write("Tata\tCDD:238825\t36.765\t68\t39\t1\t411\t614\t1\t64\t7.51e-09\t53.0\n")
            f.write("tutu\tCDD:279452\t26.016\t123\t89\t1\t183\t545\t55\t177\t3.51e-11\t62.4\n")
            f.write("toutou\tCDD:376463\t26.016\t123\t89\t1\t184\t546\t55\t177\t2.08e-12\t65.5\n")

    def _writeResultsFile(self, fileName):
        with open(fileName, "w") as f:
            f.write("Consensus_sequence,nb_copies,group,size,domains,best_hit,id,score\n")
            f.write("subgenome_blaster_Recon_333_Map_5,5,2,5326,['RT', 'RNaseH', 'MP'],Florendovirus,57.66,274.0\n")
            f.write("subgenome_blaster_Grouper_1009_Map_4,4,2,2529,['RT', 'Mplasa_alph_rch'],Florendovirus,65.67,322.0\n")
            f.write("subgenome_blaster_Piler_0.41_Map_3,3,4,5334,['RT', 'ND5', 'protease'],Unclassified_Vvin,94.92,235.0\n")
            f.write("subgenome_blaster_Grouper_213_Map_9,9,4,4002,[],Fernendo1,28.74,160.0\n")
            f.write("subgenome_blaster_Grouper_945_Map_20,23,190,1854,['RT', 'RNaseH'],Florendovirus,74.58,194.0\n")
            f.write("subgenome_blaster_Grouper_134_Map_9,9,18,2310,[],Caulimovirus,36.46,70.9\n")
            f.write("subgenome_blaster_Grouper_64_Map_20,26,18,7184,[],,0.0,0.0\n")
            f.write("subgenome_blaster_Recon_47_Map_20,20,18,1954,['RT', 'RT_Bac_retron_I', 'RNaseH'],,0.0,0.0\n")

    def _writeResultsFile_bestOnlyInOneCluster(self, fileName):
        with open(fileName, "w") as f:
            f.write("Consensus_sequence,nb_copies,group,size,domains,best_hit,id,score\n")
            f.write("subgenome_blaster_Recon_333_Map_5,5,2,5326,['RT', 'RNaseH', 'MP'],Florendovirus,57.66,274.0\n")
            f.write("subgenome_blaster_Grouper_1009_Map_4,4,2,2529,['RT', 'Mplasa_alph_rch'],Florendovirus,65.67,322.0\n")

    if __name__ == "__main__":
        unittest.main()
