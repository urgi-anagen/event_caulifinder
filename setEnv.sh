export REPET_HOST=""
export REPET_USER=""
export REPET_PW=""
export REPET_DB=""
export REPET_PORT=""

export REPET_PATH=""
export PYTHONPATH=$REPET_PATH
export REPET_JOBS=MySQL
export REPET_JOB_MANAGER=slurm
export REPET_QUEUE=slurm
export PATH=$REPET_PATH/bin:$PATH
